#pragma once

#include <cstdint>

#define DB_CO(g) ((g) > -90.0f ? powf(10.0f, (g)*0.05f) : 0.0f)

typedef enum { 
    SATYR_GAIN = 0, 
    SATYR_THRESHOLD = 1, 
    SATYR_SATURATION = 2, 
    SATYR_TYPE = 3, 
    SATYR_MAKEUP = 4,
    SATYR_INPUT = 5, 
    SATYR_OUTPUT = 6
} PortIndex;

class Satyr
{
    public:

        Satyr() = default;
        ~Satyr() = default;
        void run (const uint32_t sample_count);
        void connectPort (const uint32_t port, void* data);

    private: 
        float defaultSaturation( float input, float threshold, float saturation );
        float cubicSaturation( float input, float threshold, float saturation );
        float arctanSaturation( float input, float threshold, float saturation );
        float* gain;
        float* threshold;
        float* input;
        float* saturation;
        float* output;
        float* type;
        float* makeup;
};
