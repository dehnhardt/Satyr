#include "lv2/core/lv2.h"
#include <cmath>
#include <stdlib.h>
#include <iostream>

#include "mkl-satyr.h"

#define SATYR_URI "http://punkt-k.de/plugins/mkl-satyr"

typedef struct {
  // Port buffers
  const float* threshold;
  const float* input;
  const float* saturation;
  float*       output;
} SatyrData;


void Satyr::connectPort( const uint32_t port, void* data )
{
  switch ((PortIndex)port) {
    case SATYR_GAIN:
      gain = (float*)data;
    case SATYR_THRESHOLD:
      threshold = (float*)data;
      break;
    case SATYR_SATURATION:
      saturation = (float*)data;
      break;
    case SATYR_TYPE:
      type = (float*)data;
      break;
    case SATYR_MAKEUP:
      makeup = (float*)data;
      break;
    case SATYR_INPUT:
      input = (float*)data;
      break;
    case SATYR_OUTPUT:
      output = (float*)data;
      break;
  }
}

void Satyr::run (const uint32_t sample_count){

  for (uint32_t pos = 0; pos < sample_count; pos++) {
    switch( static_cast< int >( *type ) ){
      case 1:
        output[pos] = DB_CO(*makeup) * defaultSaturation( DB_CO(*gain) * input[pos], DB_CO(*threshold), *saturation );
      break;
      case 2:
        output[pos] = DB_CO(*makeup) * cubicSaturation( DB_CO(*gain) * input[pos], DB_CO(*threshold), *saturation );
      break;
      case 3:
        output[pos] = DB_CO(*makeup) * arctanSaturation( DB_CO(*gain) * input[pos], DB_CO(*threshold), *saturation );
      break;
    }
  }

}

float Satyr::cubicSaturation( float input, float threshold, float saturation ){
  return input - (saturation / 100.0 ) * pow( input,  3.0 ) / 3.0;
}

float Satyr::defaultSaturation( float input, float threshold, float saturation ){
  if( input > threshold )
  {
    return ( input + threshold ) / 2;
  }
  else 
  {
    float output = input * -1;
    if( output > threshold )
    {
      return ( output + threshold ) / 2 * -1;
    }
  }
  return input;
}

float Satyr::arctanSaturation( float input, float threshold, float saturation ){
  return atan( input * saturation / 10 ) * (2 / M_PI);
}

static LV2_Handle
instantiate(const LV2_Descriptor*     descriptor,
            double                    rate,
            const char*               bundle_path,
            const LV2_Feature* const* features)
{

    Satyr* satyr = nullptr;
    try 
    {
        satyr = new Satyr ();
    } 
    catch (const std::bad_alloc& ba)
    {
        std::cerr << "Failed to allocate memory. Can't instantiate mySimpleSynth" << std::endl;
        return nullptr;
    }
    return satyr;
}

// LV2 Methods

static void
connect_port(LV2_Handle instance, uint32_t port, void* data)
{
  Satyr* satyr = static_cast< Satyr* >( instance );
  if( satyr != nullptr) 
    satyr->connectPort( port, data );
}


static void
activate(LV2_Handle instance)
{}


static void
run(LV2_Handle instance, uint32_t n_samples)
{
  Satyr* satyr = static_cast< Satyr* >( instance );
  if( satyr != nullptr) 
    satyr->run( n_samples );

}

static void
deactivate(LV2_Handle instance)
{}

static void
cleanup(LV2_Handle instance)
{
  Satyr* satyr = static_cast< Satyr* >( instance );
  if( satyr != nullptr) 
    delete satyr;
}

static const void*
extension_data(const char* uri)
{
  return NULL;
}

static const LV2_Descriptor descriptor = {SATYR_URI,
                                          instantiate,
                                          connect_port,
                                          activate,
                                          run,
                                          deactivate,
                                          cleanup,
                                          extension_data };

                                          LV2_SYMBOL_EXPORT
const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
  return index == 0 ? &descriptor : NULL;
}