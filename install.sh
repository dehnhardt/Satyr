#!/bin/bash

gcc -fvisibility=hidden -fPIC -Wl,-Bstatic -Wl,-Bdynamic -Wl,--as-needed -shared -pthread `pkg-config --cflags lv2` -lm `pkg-config --libs lv2` mkl-satyr.cpp -o mkl-satyr.so
sudo mkdir -p /usr/lib/lv2/mkl-satyr
sudo cp *.so /usr/lib/lv2/mkl-satyr/
sudo cp *.ttl /usr/lib/lv2/mkl-satyr/